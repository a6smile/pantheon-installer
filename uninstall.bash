#!/bin/bash 

# checked v3

# packages 
sudo pacman -R cerbere contractor file-roller gala geary granite plank onboard orca simple-scan  

# elementary extras packages 
sudo pacman -R sound-theme-elementary elementary-icon-theme elementary-wallpapers gtk-theme-elementary

# pantheon packages 
sudo pacman -R pantheon-shortcut-overlay pantheon-terminal pantheon-calculator pantheon-calendar pantheon-camera pantheon-code pantheon-files pantheon-geoclue2-agent pantheon-photos pantheon-polkit-agent pantheon-print pantheon-screenshot pantheon-music pantheon-videos lightdm-pantheon-greeter

# switchboard 
sudo pacman -R switchboard switchboard-plug-a11y switchboard-plug-about switchboard-plug-applications switchboard-plug-bluetooth switchboard-plug-datetime switchboard-plug-desktop switchboard-plug-display switchboard-plug-keyboard switchboard-plug-locale switchboard-plug-mouse-touchpad switchboard-plug-network switchboard-plug-notifications switchboard-plug-parental-controls switchboard-plug-power switchboard-plug-printers switchboard-plug-security-privacy switchboard-plug-sharing switchboard-plug-sound switchboard-plug-user-accounts

# wingpanel
sudo pacman -R wingpanel wingpanel-indicator-bluetooth wingpanel-indicator-datetime wingpanel-indicator-keyboard wingpanel-indicator-network wingpanel-indicator-nightlight wingpanel-indicator-notifications wingpanel-indicator-power wingpanel-indicator-session wingpanel-indicator-sound

# build files will be added letter ! 
# aur packages list 
sudo pacman -R cerbere-git gala-git pantheon-applications-menu-git pantheon-dpms-helper-git pantheon-default-settings switchboard-plug-elementary-tweaks-git elementary-wallpapers-git pantheon-session-git gconf libhandy1 libhandy gala-git

# checking orphans 
pacman -Qtdq

# cleaning orphans 
sudo pacman -Rns $(pacman -Qtdq)
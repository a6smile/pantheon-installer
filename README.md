# Pantheon for Arch Linux 

A pantheon desktop installer bash script for arch linux and arch linux based distributions.

[elementary OS](https://elementary.io/) releases are derived from [Ubuntu's](https://wiki.archlinux.org/index.php/Arch_compared_to_other_distributions#Ubuntu) LTS releases, typically trailing Ubuntu's cycle by a few weeks or  months. However, its constituent packages are updated continuously. See  the official [github repository](https://github.com/elementary) and consult their [community slack](https://elementarycommunity.slack.com/).



![scren_1](images/desktop.png)



![scren_1](images/neofetch.png)





> ## ***Note***
> 
> I've added **lightdm** theme here. If you're not a **lightdm** user then just remove `lightdm-pantheon-greeter` package after run `install` script. 



## Installation 

1. Clone or Download Installer from source.
2. Run Installer ~ `$ sudo ./install`
3. Then run *AUR* builder ~ `$ ./build`
4. After building *AUR* packages copy and pest those commands 
5. `$ git clone https://aur.archlinux.org/pantheon-session-git.git`
6. `$ cd pantheon-session-git`
7. `$ nano PKGBUILD` and then find `wingpanel-git` and rename it to `wingpanel`.  Then save and quit
8. `$ makepkg -si`  
9. Reboot your computer and hope you'll find pantheon in your lightdm session menu. 



> ## ***Note***
> 
> If you've got any missing dependency error then search for this package in arch repository. If you found the missing package then try to build manually. To build manually follow steps from **5** to **8** and **skip the 7th step**



## Manually Build Installation

1. First run installer `$ sudo ./install`
2. Install some extra packages -  `# pacman -S libhandy xdg-user-dirs-gtk`
3. Remove some conflict packages - `# pacman -R pantheon-applications-menu elementary-wallpapers`
4. After that redirect your location in **/tmp** directory using `$ cd /tmp ` command. 
5. Building **gconf** 
	* `$ git clone https://aur.archlinux.org/gconf.git`
	* `$ cd gconf`
	* `$ makepkg -si`
6. Building **libhandy1**
	* `$ git clone https://aur.archlinux.org/libhandy1.git`
	* `$ cd libhandy1`
	* `$ makepkg -si`
7. Installing **Elementary Tweak Tools**
	* `$ git clone https://aur.archlinux.org/switchboard-plug-elementary-tweaks-git.git`
	* `$ cd switchboard-plug-elementary-tweaks-git`
	* `$ makepkg -si`
8. Installing  **Default settings** for Pantheon
	* `$ git clone https://aur.archlinux.org/pantheon-default-settings.git`
	* `$ cd pantheon-default-settings`
	* `$ makepkg -si`
9. Installing Elementary **Wallpapers**
	* `$ git clone https://aur.archlinux.org/elementary-wallpapers-git.git`
	* `$ cd elementary-wallpapers-git`
	* `$ makepkg -si`
10. Building pantheon session dependency 
	- **cerbere-git**
		* `$ git clone https://aur.archlinux.org/cerbere-git.git`
		* `$ cd cerbere-git`
		* `$ makepkg -si`
	- **gala-git**
		* `$ git clone https://aur.archlinux.org/gala-git.git`
		* `$ cd gala-git`
		* `$ makepkg -si`
	- **pantheon-dpms-helper-git**
		* `$ git clone https://aur.archlinux.org/pantheon-dpms-helper-git.git`
		* `$ cd pantheon-dpms-helper-git`
		* `$ makepkg -si`
	- **pantheon-applications-menu-git**
		* `$ git clone https://aur.archlinux.org/pantheon-applications-menu-git.git`
		* `$ cd pantheon-applications-menu-git`
		* `$ makepkg -si`
11. Building **pantheon session**
    * `$ git clone https://aur.archlinux.org/pantheon-session-git.git `
	* `$ cd pantheon-session-git `
    * `$ vim PKGBUILD` and then find `wingpanel-git` and rename it to `wingpanel`.  Then save and quit.
	* `$ makepkg -si`
12. Reboot your computer and hope you'll find pantheon in your lightdm session menu. 
        

## Uninstall 

Just need to run `$ sudo ./uninstall.bash` 



## Tweak 

* [Speed Indecator](https://aur.archlinux.org/packages/wingpanel-indicator-sys-monitor-git/)
* [To Do after install pantheon desktop](https://gist.github.com/suberb/4635a6c338f0f66b63c0f502859e5b42)



## Fixing Error's! 

* **How to fix display resolution** - Run `$ xrandr` on your terminal. It outputs current screen resolution as well as all available solutions. Change your ratio `$ xrandr--size 16:9` after then go to display settings and apply your preferred resolution. 

* **How to change default terminal font** - To change font in the Terminal you need `dconf-editor`. Run it and go to path `org` > `gnome` > `desktop` > `interface` > `monospace-font-name`.  Enter name of desired font and font size. For example `Anonymous Pro 11`

![donf_editor.png](images/donf_editor.png)

* **Root** ~ Do not run the script as *root*, Then you'll have to face error like `==> ERROR: Running makepkg as root is not allowed as it can cause permanent,
    catastrophic damage to your system.`
    
* **cerbere-git** ~ If you'll have `cerbere-git and cerbere are in conflict. Remove cerbere? [y/N]` then press **y**

* **gala-git** ~ If you'll seen `gala-git and gala are in conflict. Remove gala? [y/N] ` then go with **y**

* **pantheon-dpms-helper-git** ~ Again if you've seen `pantheon-dpms-helper-git and pantheon-dpms-helper are in conflict. Remove pantheon-dpms-helper? [y/N]` while building then go with **y** 





Sources ~ 

1. [Elementary Github Repository](https://github.com/elementary)
2. [Pantheon Arch Wiki](https://wiki.archlinux.org/index.php/Pantheon) 
3. [Vitux - Change resulation using *xrandr*](https://vitux.com/how-to-change-screen-resolution-through-the-ubuntu-terminal/)
4. [Ubuntu HandBook - Save *xrandr* for next boot](http://ubuntuhandbook.org/index.php/2017/04/custom-screen-resolution-ubuntu-desktop/)
5. [StockExchange - Change font](https://elementaryos.stackexchange.com/questions/1149/how-can-i-change-the-default-terminal-font)
6. [eOS Theme](https://github.com/btd1337/eOS-Sierra-Gtk)



## **Farhan Sadik**